+++
date = 2023-09-11T07:53:34Z
title = "Payroll"
categories = ["payments"]
+++

## Sure Pay Roll

For US employees, we use the Sure Pay Roll service, which properly handles all
employee and employer tax deductions. It does not include any state-imposed
insurance, such as workers comp or disability. At present, we do not have any
employees with such state requirements.

We are sent a reminder twice a month to login to Sure Payroll and approve
payroll. And we are billed monthly for the service.

## Citi Mobile

For international employees, we login to Citibanks' mobile portal and:

 * Click "Payments and Transfers -> Payments Dashboard"
 * Click "Wires and Transfers"
 * Click "Send a wire transfer"
 * Click "Setup a wire transfer"
 * Click Foreign
 * Click "Use Foreign Model"
 * Click the name of the employee to pay
 * For currency, click "Set to US Dollars"
 * Enter amount and Send
