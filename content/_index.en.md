+++
date = 2022-11-24T13:53:34Z
title = "Welcome"
categories = ["orientation"]
weight = -100
+++

# Welcome

*Want a quick view of everything? [See all pages organized by category.](/categories)*

Welcome to the [May First Movement Technology](https://mayfirst.coop/)
office documentation. 

This site is intended for May First staff people who are responsible for office
administration - ensuring that member payments are properly coded, bookkeeping
is properly kept, and general office administrative tasks are handled.

## Membership Organizing 

The primary task is using the membership life cycle as an opportunity for
organizing our members. These tasks rely heavily on our [CiviCRM
database](/civicrm). They include:

 * [Introduce](/introduce) May First to the movement.
 * [Welcoming new members](/new) who join and ensuring they know what May First
   is and how to get involved.
 * [Answering questions](/answer) from members or referring them to the people
   who can answer those questions
 * [Engaging with members](/engage) to deepen our relationship
 * [Following up](/followup) with members who have fallen behind on their dues
   to find out why and offer options for payment reduction.
 * [Members who pay in MXN](/mxn) (Mexican pesos) are handled by our sibling
   organization, Primero De Mayo/Enlace Popular. 

## Payments in and out 

 * [Managing payments in](/payments-in) - all credit card or Pay Pal membership
   payments are automatically applied in our CiviCRM database, however checks
   must be handled manually, and when something goes wrong, intervention is
   needed to set things right.
   * [Anonymizing payments in](/anonymizing-payments) - Members have the right 
   to request that their payment information be anonymized.
 * [Making payments out](/payments-out) - most of our expenses are regular
   monthly expenses and they are automated, however some must be made as
   one-offs.

## Bookkeeping

In addition, the office workers are responsible for maintaining our books,
which includes:

 * Ensuring our income and expenses are recorded in out [GNUCash accounting software](/gnucash) (this step is mostly automated) 
 * [Reconciling our books](/reconciling)

