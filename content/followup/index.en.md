+++
date = 2022-11-24T13:53:34Z
title = "Follow Up with our membership"
categories = ["membership"]
+++

Members fall behind on their dues regularly. Rather than see this as debt
collection, we can instead see it as an opportunity to get to know our members
better.

## Timeline:

 * **Automatic notices:** Members begin receiving automatic notices about their
   membership expiring two weeks prior to their membership end date. If their membership
   is configured to auto-renew, their notice includes that information.

   If they do not renew their membership, they will receive a notification every two
   weeks for two months. Their last notice will tell them that their membership will
   be disabled.

   Technically, this two month period is a grace period (and their membership
   is marked as "grace" during this time). They are still considered "current"
   members and are still considered to be in good standing.

 * **Expired:** After two months, their membership status changes to "expired."
   At this point they are no longer considered to be current members. And, we
   may disable their hosting resources. We should regularly monitor for members
   who have expired and as soon as they enter this status. 

   *Before Disabling a membership*:

   * **Check the "Notes" section of the CiviCRM record.** This section is a log of
     our interactions with the member and should include past conversations.

   * **Check resource usage.** It's useful to check if their hosting resources
     are still using May First. If a membership has expired, they don't respond
     to email, and they are no longer using our services, it's an easy step to
     disable their membership (if we made a mistake, we can always re-enable
     it). On the other hand, if they are using our servers, now is an opportunity to
     send a more personal email (maybe they are ignoring form emails but will
     respond to a personal one?). It's also an opportunity to research them,
     learn more about what they do. You may also need to find different contact
     information - or try reaching them via social media.

   If you are successful in reaching them, it's an opportunity not just to tell
   them to pay, but to also let them know what activities we have planned and
   invite them to get more involved.

   If you have no luck reaching them, then you need to disable their hosting
   details.

 * **Disabled** Disabling means clicking on the hosting order section of the
   control panel and disabling every hosting order.

   Next, in the CiviCRM database, edit their membership, choose "Status
   Override Permament" and change their membership status to "Disabled."

 * **Coming back from Disabled:** There are two ways to come back from being
   disabled.

   * They pay. If this happens, the system should automatically change their CiviCRM
   membership status to "Current" and unset the permanent override setting. And, we
   should get an email letting us know that we have to manually enable their hosting
   orders in the control panel.

   * They get in touch with us and provide a date that they will pay. If this
   happens, we generally re-enable their hosting orders (even if they have not yet
   paid) *and* edit their membership in CiviCRM so as not to override the status
   any longer (they will go back to expired). Also, be sure to add a "Note" to the
   CiviCRM record indicating what happened. If they don't pay, they will remain in
   the expired state where we will have to disable them again.

 * **Cancelling** A membership should be cancelled if they request to end their
   membership or they have been disabled for more than 3 months.

   A list of members that have been disabled for more than 3 months is available
   via the `Memberships -> Members disabled for 3 months or longer` menu item.

   To delete a membership:

     * In the control panel, delete each hosting order one by one.
     * In the control panel, search for the member name and then click
       the delete link next to the membership
     * In CiviCRM, edit the membership, choose to override manually permanently,
       and select "Cancelled"
     * In CiviCRM, check the contributions tab and the recurring payments sub-tab.
       If there is an active recurring contribution, cancel it.

   If you cancel a membership that has an active recurring contribution, you will
   get a warning that will remind you to cancel their recurring payment. We don't
   want to collect monty from a member after they have cancelled!

   ![Warning when you cancel a membership with a recurring payment](cancel-recur-contribution.png)

## Maintenance

In addition to the timeline, there is some regular maintenance that is required.

### Ensuring all members have a Membership Admin relationship 

Contacts assigned the "Membership Admin" relationship receive notices when it's
time for a member to pay their dues.

But, sometimes all the membership admin contacts have been deleted, or the
primary email address for the only membership admin contact is bouncing.

#### Detecting that there is a problem

When it is time to send a membership renewal email and there is no working
membership admin contact, the database alerts us at login time with the
"Scheduled Job Failures" message:

![Initial notice](initial-notice.png)

You can find more information about this problem by either:

1. Click "View details and manager alerts" and then click "View Job Log":

![View Job Log](view-job-log.png)

OR

2. Click `Administer -> System Settings -> Scheduled Job` and scroll to the job
called "May First Send Renewal Reminders (Daily)" and click "View Job Log"

#### Fixing the problem

The job log provides the exact member ids that caused the problem:

![Find member ids](member-ids.png)

You can then click `Memberships -> Find Members` and plug those ids into the
search to find them:

![Find member id](search-by-member-id.png)

Once you find the membership, there is no single answer as to what you should do. Some options are:

1. **Use Alternate Email** If there is a membership admin contact with a
   bouncing primary email that has a second email, you can edit the email
   address and make the alternate email address the primary one.

2. **Promote or contact regular membership contact** If there is a regular
   "Member contact" that has a working email, you can create a new "Membership
   Admin" relationship for that contact to promote them OR you can simply email
   them and ask them what to do.

3. **Lookup on the web** You can look for a web site address for the given membership and see if they
   have a published "info@" email address that can be used. Than create a new
   contact (first name: Admin, last name: Contact) with this email and create
   both a Membership Admin relationship and a Membership relationship.

4. **Ask us** You can ask other workers if they are familiar with this member or have
   contact with them.

5. **Check control panel** If the member uses May First for their email, you can look up their hosting
   order in the control panel and see if there are any active email addresses
   that can be used.

6. **Uncheck bounce checkbox** If the contact's email address is bouncing and
   you think it was a temporary problem, you can edit the email address and
   uncheck the bounce check mark so we will try to deliver to the email account
   again.

Once you have fixed the problem, there is nothing left to do. The next day, the
schedule job will run again and this time it will find a contact to email and
the job should complete successfully.
