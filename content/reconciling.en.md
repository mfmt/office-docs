+++
date = 2022-12-18T13:53:34Z
title = "Reconciling and EOY steps"
categories = ["bookkeeping"]
+++

# Reconciling

At least twice a year we reconcile our various accounts:

 * Checking Account (citibank)
 * Stripe Account (for credit card contributions)
 * Pay Pal

The steps to reconcile are:

1. Login to each service and download the monthly statement for each month Each
   statement is placed in our Nextcloud accounting folder for the proper year.
   * Stripe: From left side bar click `Reporting -> Reports -> Balance Summary`. Generate a new report for each month. Click print and print to a PDF.
   * Paypal: `Activity -> All Reports -> Statements - monthly and custom`. Request and then download each monthly report.
   * Citibank (citi mobile web site, not citibusiness): `Services -> Statements and Documents -> Statements`. Download each month.
2. Login to our Payroll service and download the month to date payroll reports
   for each month and place them in our Nextcloud accounting folder. 
   * Click `Reports -> Payroll Reports -> Month to date`. Select each month, click "Update Report" and download the PDF. 
3. Ensure income and expenses are imported properly from our CiviCRM installation using the [GnuCash Importer script](https://code.mayfirst.org/mfmt/gnucash-import-scripts/-/blob/main/gnucash-importer.py)
4. Enter the Payroll data using the [GnuCash Payroll Importer script](https://code.mayfirst.org/mfmt/gnucash-import-scripts/-/blob/main/gnucash-payroll.py)
5. Reconcile each bank account in GnuCash.

# End of Year

Each January, after reconciling, we take a number of steps to close out our books.

1. **Depreciation.** In GnuCash we have a "Fixed Assets" account with our server purchases. Divide the cost of the server by 5 years and add a new line in the Depreciation account under the server in question for that amount. If it's value is 0, you don't have to do this any more. It can be dated December 31 of the previous year.
1. **Deferred Revenue.** If we get a one year membership paid in, for example, December, we can only count one month of revenue for last year. The rest is deferred to the next year. We account for this in the Deferred Revenue - Dues liability account. Every year:
  * Account for the income we deferred the previous year. Create one entry called "YYYY (last year) Membership dues deferred from YYYY (2 years ago)" - it should be equal to the line from the previous year titled "YYYY (2 years ago) Membership dues deferred to YYYY (last year) and the amount put in the "Decrease" column and the Transfer column should be "Income:Membership Dues - Combined". When you enter this amount, the balance in the "Deferred Revenue - Dues" account should be zero.
  * Lookup the amount from the prevous year that should be deferred to this year. You need to access a SQL shell in our CiviCRM database and run this query: `SELECT SUM(deferred_amount) FROM civicrm_mayfirst_contribution mc JOIN civicrm_contribution c ON mc.contribution_id = c.id WHERE invoice_start_date BETWEEN '2024-01-01' AND '2024-12-31' AND currency = 'usd';` (assuming the accounting year is 2024 - change as needed). 
  * Defer income to next year. Create one entry called "YYYY (last year) Membership dues deferred to YYYY (this year)". The Transfer account should be "Income:Membership Dues - Combined" and it should go in the Increase column.

 
