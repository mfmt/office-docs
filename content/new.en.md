+++
date = 2022-11-24T13:53:34Z
title = "New Members"
categories = ["membership"]
+++

New members join May First by filling out our [new membership
form](https://outreach.mayfirst.org/civicrm/public/save#/mayfirst/save), which
triggers an email to everyone in the "Receive New Membership Notifications"
group in our CiviCRM database.

After filling out a membership form, the contact is added to the database
with a membership in the "Requested" state.

Office workers can click the `Memberships -> Review Pending May First
Memberships` menu item to review pending memberships.

After clicking "process" you are presented with a page listing three steps:

1. **Convert their membership status from "Requested" to "Grace".** This step
   officially approves them for membership. They are in the "Grace" status
   since they have not yet paid, but they will be considered a member in good
   standing for the next 2 months.

2. **Send a welcome email.** This step lets them know they have been approved.
   And, a templated email is appended to your welcome message that provides them
   with a boiler plate welcome message, details about their membership and a link
   to pay their dues. After being sent, you can see the message in the Activities
   tab of their contact record.

3. **Setup Control Panel items.** This step both sets up their hosting details
   and also triggers a second email that allows them to click a link and set
   their initial password.

