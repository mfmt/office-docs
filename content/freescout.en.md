+++
date = 2024-01-26T13:53:34Z
title = "Using Freescout"
categories = ["membership"]
+++

Freescout is our ticketing system, available via [https://freescout.mayfirst.org](https://freescout.mayfirst.org/).

The office is responsible for tickets that arrive in the "May First Office" mailbox.

## Bounces

We get a lot of bounces.

We take different actions depending on the type of bounces.

### Bad email address in our system.

If the message is from `members@mayfirst.org` AND the error message seems like
a permanent error, then search for the address in CiviCRM and in the control
panel and remove/replace it with another email address from the same membership
or research (search their web site) to find a working email address.

Examples:

```
Mail Delivery System
To: members@mayfirst.org

This is a bounce message.

This is the mail system at host 046.bulk.relay.mayfirst.org.

I'm sorry to have to inform you that your message could not
be delivered to one or more recipients. It's attached below.

For further assistance, please send mail to postmaster.

If you do so, please include this problem report. You can
delete your own text from the attached returned message.

The mail system

<xxx@xxx.org>: host aspmx.l.google.com[142.250.105.27] said:
550-5.2.1 The email account that you tried to reach is inactive. For more
550-5.2.1 information, go to 550 5.2.1
https://support.google.com/mail/?p=DisabledUser
00721157ae682-6f75787728fsi40490557b3.65 - gsmtp (in reply to RCPT TO
command)
```

Note - the bounce message was sent to `members@mayfirst.org`, so that is most likely the email address that sent it.

The error message is: "The email account that you tried to reach is inactive." which suggests that no matter how many times we resent this message, it will probably continue bouncing. In contrast, a vacation message or over quota message suggests that it's a temporary problem and we should delete/ignore this bounce message.

## Bounced email address from mx-verify-domain

If the message was sent to an address that starts with `mayfirst-verify-mx-domain`, then
you can safely ignore/delete.

When a new domain name is added to our control panel we always send a test
email to see if we are supposed to deliver email to that domain. If the domain
is *not* yet pointing to our servers, we will get a bounce. These can always be
ignore/deleted.


Example:

```
This is the mail system at host 066.bulk.relay.mayfirst.org.

I'm sorry to have to inform you that your message could not
be delivered to one or more recipients. It's attached below.

For further assistance, please send mail to postmaster.

If you do so, please include this problem report. You can
delete your own text from the attached returned message.

The mail system

<mayfirst-verify-mx-domain@xxxx.com>: host
b.mx.mayfirst.org[204.19.241.86] said: 554 5.7.1
<mayfirst-verify-mx-domain@xxxx.com>: Relay access denied (in reply
to RCPT TO command)
```

### Errors from verify-mx-domain

Sometimes something goes wrong with our script. The bounce message might look like this:

```
<mayfirst-verify-mx-domain@c.mx.mayfirst.org> (expanded from
<mayfirst-verify-mx-domain@xxxxx>): Command died with status 1:
"/usr/local/sbin/mf-verify-mx-domain". Command output: Failed API call to
https://members.mayfirst.org/cp/api.php with data {'action': 'mx_verify',
'where:key': 'xxxxxx'} Failed to find passed in key.
```

Notice the part that says "Command died with status 1" - that's not a good
sign. Assign to support to investigate these.


