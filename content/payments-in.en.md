+++
date = 2022-11-24T13:53:34Z
title = "Payments In"
categories = ["payments"]
+++

## The two most common ways payments are sent to May First:

 * **Credit Card** - these payments are either made as a donation via our donation
   page (and coded in our CiviCRM database with the Financial Type "Donation")
   or made via our membership dues page and coded as "Membership Dues".
   
   All credit card payments are handled via the Stripe payment processor, which
   has a Dashboard that we can log in to.

 * **PayPal** - membership dues may also be paid via Pay Pal. We can log into our
   PayPal account as well.

Please see our [refunds](/refunds) document to learn how to refund over payments made
via credit card or PayPal.

-----

A few notes: 

Both Credit card and PayPal payments are (typically) automatically applied to
member records in [CiviCRM](/civicrm) (or automatically applied as donations in
CiviCRM) and automatically imported into our [GnuCash](/gnucash) accounting
software. No action is required on our part for these payments.

**Except**... Some people try to pay their dues via our membership system but
it doesn't work. See the [one off credit card renewals
page](/civicrm/#one-off-credit-card-renewals) for information on how to handle
that situation.

----

## Additional methods to pay us:

 * **Checks** - Some members mail checks our to [virtual postal mail
   address](/virtual-postal-mail). These should be deposited to CitiBank via a
   [Virtual Postal Mail check deposit request](/virtual-postal-mail) and
   applied to the correct membership (see the
   [CiviCRM](/civicrm#manual-renewals) page for more details). 

   Once entered into CiviCRM, they will be automatically imported into GnuCash.

   Sometimes we are paid for special projects via check, in which case they
   must be entered direclty into our accounting software (see
   [GnuCash](/gnucash)).

 * **EFT** - Some payments come via Electronic Funds Transfer - directly into
   our checking account. Some international members request or SWIFT code to
   make this transfer. These must be manually entered into CiviCRM if they are
   membership related (and from there they will be automatically entered into
   GnuCash). Or, if they are not member-related, they must be manually entered
   into GnuCash.

 * **Bill.com** - Some members use the bill.com site to send us payments. We
   have an account that automatically moves
   money sent to us via bill.com into our checking account, so this method is
   similar to EFT. We generally get an email when a payment is made via
   bill.com, which is our signal to manually enter it into our CiviCRM
   database.
   * Our bill.com PNI Number is: 0196352100826314
   * Our profile: [https://app.bill.com/network/mayfirstmovementtechnology](https://app.bill.com/network/mayfirstmovementtechnology)
