+++
date = 2022-11-24T13:53:34Z
title = "Answering questions"
categories = ["membership"]
+++

Members and prospective member ask general questions using either the
`members@` or `info@` email addresses (support questions are asked via the
`support@` address).

The `members@` address is the from address for most membership related
automated email messages, like notices to pay your dues, etc.

The `info@` address is the from address used for mass mailings sent to members.

Frequently asked memberhip questions include:

### What's included with our membership? Can I do X?

Both existing members and people interested in becoming members often have
questions about hosting benefits. Sometimes these questions are very basic and
other times they can be highly technical.

### Can I talk to someone?

Often prospective members want to talk to a human to gauge whether or not they
want to commit to a membership. Committing your Internet resources to a new
organization is a big and consequential leap that's hard to evaluate based on a
web site.

### Something went wrong with the membership payment

These are all different and require escalation to someone who can debug our
CiviCRM installation.

### Payment is coming, or can't make a payment

Make a note in the database that their payment is coming. Or, request that they
submit a formal request for a dues reduction.

### Cancel membership

When asked to cancel a membership, it's important to perform a cursory check
first. If their hosting resources appears to currently be in use, it's important
to follow up letting them know exactly what services will be lost if they cancel.

Even if no services will apparently be lost, always respond with the
"Membership Admin" relationship email in the cc: field and ask for a
confirmation that they have downloaded all of their data. This step both
ensures they have their data and understand that canceling their membership
will result in permanently deleted data and also by cc:ing the primary contact
it ensures that this request is not forged or asked by someone without
authority.

### Change contact information

Please see [who do we contact](/civicrm/#who-do-we-contact-about-a-membership)
for more details on how to properly assign contact information.

### Facturas and payments in MXN

Any question related to payments made in MXN (Mexican pesos) should be forwarded to
Primero de Mayo/Enlace Popular (see [Members who pay in MXN](/mxn)).
