+++
date = 2022-11-24T13:53:34Z
title = "CiviCRM"
categories = ["membership"]
+++

Our primary database for both member communication and tracking membership
dues uses the [CiviCRM](https://civicrm.org/) software and is located at:
https://outreach.mayfirst.org/.

A general [CiviCRM users manual is
available](https://docs.civicrm.org/user/en/latest/). This page provides
limited information that is highly specific to our instance.

## Entities

In CiviCRM, there are several entities:

 * **Contacts** - Each member of May First has one contact record that
   represents their membership. 

   If they are an individual member, it's a Contact record of the type
   "Individual." If it's an organization, it's a contact record of the type
   "Organization". The only difference between an individual member and an
   organization member is that organization members get two votes.

   There are also many contact records in the database that are not members.
   They may be people who have registered for one of our webinars, former
   members, individuals related to members (see relationships below) etc. 

 * **Relationships** - One contact record can have one or more relationships to
   other contact records. Each relationship can have a "type". The two main
   relationship types we use are:

     * *Membership Admin Contact:* these contact records get the renewal
       reminder email and have the privilege of editing their membership record
       to add new resources or change who the related contacts are.

     * *Membership contact:* these contacts receive membership email
       announcements and voting tokens.

   *Note:* An individual membership contact can have a relationship to itself.
   All member contacts must have at least one Membership Admin contact so we
   can notify them when their membership expires.

   *Another Note:* Why do we have to create both a "Membership Admin Contact"
   and a "Membership contact" relationship? Answer: Our membership mailings
   only go to the "Member Contact" relationships.

   Also, because at any time we may delete the "Membership Admin Contact"
   relationship because they contact no longer wants to receive billing
   information, but we still want them associated with the memberhsip.
  
 * **Memberships** - A contact that is a member must have at least one
   membership listed in the Membership tab. There are only two types of
   memberships:

    * *Annual*: pay dues once a year
    * *Monthly*: pay dues every month. Only members paying in USD can pay
      monthly and they must pay via credit card or Paypal with the recurring
      option turned on so we don't have to process monthly checks by hand. 

 * **Contributions** - When a member pays their membership dues, a contribution
   record is automatically created in the database and their membership record
   end date is advanced either by one month or one year depending on their membership
   type.

### Entities in relation to the control panel

We have two systems that track members: The CiviCRM database and the Control panel.

We link the two using the `member_id` in the control panel and the `external_identifier`
in CiviCRM. If the `member_id` in the control panel is "1234", the corresponding
contact in CiviCRM would have an `external_identifier` of "member:1234".

![](control-panel-civicrm.drawio.png)

## Who do we contact about a membership?

A contact record may have contact information associated with it (an email
address, a postal address, etc).

For contacts with a membership:

 * The postal address is used to tell us the geographic distribution of our
   membership. But we never send anything by postal mail.

 * We only send email or SMS messages to "Member Admin Contacts" (billing
   reminders) or "Member Contacts" (all other membership announcements) of
   members. We never send email to the member contact itself (see below for an
   exception).

 * Individual members have a "Member Admin Contact" and "Member Contact" relationship to
   themselves. In this case, we *do* in practice send an email or SMS to the member contact, but only
   because of it's relationship.

Some organizations offer a generic email like `bills@myorg.org` that is not
associated with a name. Our practice is to create a contact with the first name
"Bills" and last name "Myorg" (or something to that effect) with the given
email address and make that contact a "Member Admin Contact" and "Member
Contact" for the organization. 

## USD vs MXN members

A membership can be coded to pay in MXN or USD. MXN membership are
administratively handled by Primero de Maya/Enlace Popular coop.

*Note: A contact record can also be coded as MXN or USD. This should probably
be removed because it's the membershp record that counts.*

*Note again: A contact record can also be coded to prefer Spanish over English.
This setting controls what language their communications is sent in, but has no
relationship to the currency the use to pay their membership.*

These documents are limited to members who pay in USD.

## Renewing a membership

### Automated renewals

The main way memberships are renewed is fully automated. When a member pays for
the first time, the "auto renew" checkbox is checked by default and the member
pays either by Credit Card (which is handled by the Stripe payment processor)
or by PayPal. In either case, PayPal and Stripe will automatically charge their
account when the renewal is up and notify our database which will update their
membership.

**When an automatic renewal is started *after* the due date**

If a member renews after their due date and chooses to automatically renew,
then they will always be late on their dues.

To address this problem we take one of two steps:

1. If the payment is less than 15 days after the membership due date, we adjust
   the membership due date, extending it so it falls after the next membership
   renewal date.

2. If the payment is more than 15 days after the membership date, we cancel the
   recurrance and notify the member. We explain that we cancelled the recurring
   payment because it will be late and let them know they will be reminded when
   they are due and ask them to start a new recurring payment when they next pay.

**How to cancel a recurring contribution.**

1. Lookup the contact record
2. Click the Contributions tab
3. Click the "Recurring Contributions" sub tab
4. Click Cancel:

!["Screen shot of cancel screen"](civicrm-cancel-recur.png)

5. Then, set the options to cancel, provide a reason and notify the user:

!["Screen shot of cancel reason screen"](civicrm-cancel-recur-reason.png)

### Manual renewals

If a member pays by check, bill.com, EFT or any other way that does not
automatically credit their membership, their membership should be renewed by:

 * If they pay by check, ensure you have a digital image of the check (when checks are sent to our virtual post office, you can use the scanned version of the check)
 * In the database, click `Memberships -> Manually Renew Memberships` 
 * In the "Choose the membership to renew" field, start entering the first few letters of the members name and then select the member. 
 * Enter the proper `Date received` and `Payment Method`  (use EFT for bill.com)
 * If you selected "Check" be sure to enter the check number.
 * The `Status` should be completed.
 * If they are paying for more than one term, adjust the `Number of terms to advance membership` field.
 * Enter the total amount *and the `New Expiration Date` will be automatically set* - so, be sure to check it to make sure it looks correct.
 * Click `Save Payment`
 * If payment method is check, then click the link to upload the receipt to the contribution record
   * On the contribution record edit screen, click the "Browse" button next to the `Transfer Receipt` label.
   * Click `Save` to save the check image with the contribution.

Note: If the amount they are paying is exactly the amount that they owe, then
their membership will be advanced exactly one period. If it's slightly more or
less, then it will be pro-rated by advanced their expiration date less or more
then a full term.

### One off credit card renewals

Sometimes a member wants to make a membership contribution via credit card, but
they can't use the normal process because:

 * They have a recurring contribution already established (to prevent over
   payments our regular system will stop you from making a membership payment
   if you have a recurring payment that is active). 

 * A member wants to pay a different amount. Our normal system does not give
   you the option to choose the amount to pay. You will always pay the amount
   of one term of membership. Sometimes a member might have extra money and
   want to pay ahead.

To accomodate these situations, we have a special [one time membership
contribution
page](https://outreach.mayfirst.org/civicrm/contribute/transact?reset=1&id=4).
When a donation is made, the members@mayfirst.org email address will be
Bcc'ed on the thank you email, alerting us to the contribution. 

When this happens, we need to take the following steps:

1. Find the contribution in the database. You should be able to use the
   information in the email notice to locate it.
2. Click the "view" link next to the donation. There should be a value in the
   "Note" field that indicates which membership it should be applied to. If
   there is not sufficient information, then we need to follow up with the
   donor to find out how to apply it.
3. In list view (when you see all contributions as a list), click the "..."
   menu and choose "Move Contribution". Move the contribution to the membership
   it should be applied to.
4. Manually adjust the membership end date to reflect the new payment.

## Sending email

We sending email to our members using the CiviMail feature of our database.

We typically send to two groups (with each group sub-divided into English and Spanish speakers):

 * **COM: Members** - this group comprises all contacts that are related to
   members. We use this group infrequently to notify members about news or
   events that are only of interest to members. Since we use this list to
   inform members about the membership meeting, voting etc. we do not want
   members to unsubscribe so we send to it sparingly.
 * **COM: World** - this group includes everyone in the database with an email
   address.  We send all other announcements to this group.

Every message has an english version (sent to the english sub-group) and a
spanish version (sent to the spanish sub-group).
