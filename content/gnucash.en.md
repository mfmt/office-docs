+++
date = 2022-12-18T13:53:34Z
title = "GNUCash"
categories = ["bookkeeping"]
+++

Nearly all of our [income](/payments-in) comes from Membership dues, which are
already entered into our CiviCRM databases. Similarly, our practice is to enter
all of our [expenses](/payments-out) into CiviCRM as well.

To avoid entering the same data twice by hand, we automatically import these
transactions from CiviCRM to GNUCash using our  [GnuCash Importer
script](https://code.mayfirst.org/mfmt/gnucash-import-scripts/-/blob/main/gnucash-importer.py).
