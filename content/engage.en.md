+++
date = 2022-11-24T13:53:34Z
title = "Engaging with our membership"
categories = ["membership"]
+++

Our democratic culture is based on member participation in our activities and
our participation in our members' activities. Additionally, participating in
movement activities outside our membership is a vital way to introduce May
First to new groups and also run into members who also participate in those
spaces.

## Email lists

Sending mass email about our activities is the primary way we let our members
know what we are doing and providig options for them to get involved. More
information on how to send email is on our [CiviCRM](/civicrm) page.

## Work team meetings

Our TIAS and Engagement Team meeting are important opportunities to get to know
our most active members and expose those members to other activities they may
not be aware of.

Additionally, if a work team is engaged in a particular activity, it's an
opportunity to personally notify members who we know are engaged in that work
to participate.

## One off meetings

Some members ask for meetings to learn more about May First. Often they have
technical questions, but it's a great opportunity to give them a bigger picture
of what we do and look for opportunities to collaborate.

## Member and movement events and activities

We can't attend every activity of every member, but showing up at member events
is a good way to deepend our connection.

Additionally, showing up at events related to movement technology, even when
organized by non-members, is a good way to introduce May First to new people
and often we will have an opportunity to engage with members who are also
attending.
