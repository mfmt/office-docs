+++
date = 2023-10-12T11:00:00Z
title = "Bank Transfer"
categories = ["payments"]
+++

All Bank transfers are made via the [Citibank main site](https://citi.com/).

We made two kinds of transfers:

 * **Domestic:** typically for paying interpreters or any other vendor who does not want to be paid via credit card or Pay Pal
 * **Foreign:** Paying staff who are out of the country

## Domestic

 1. Payments and Transfers -> Payments Dashboard
 1. Click Pay Your Bills
 1. Click Pay next to existing person OR Add New Payee

## Foreign

 1. Payments and Transfers -> Payments Dashboard
 1. Click Wires and Transfers
 1. Click "Send a Wire Transfer" button
 1. Wait for it to load...
 1. Click "Set up a Wire Transfer"
 1. Click "Foreign"
 1. Click "Use Foreign Model" button
 1. Select Jes
 1. Currency: Change to USD
 1. Amount: Enter proper amount
 1. Click continue
