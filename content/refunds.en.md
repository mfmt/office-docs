+++
date = 2023-11-27T13:53:34Z
title = "Refunds"
categories = ["payments"]
+++

Both PayPal and Stripe allow us to refund payments.

In both cases, the easiest method is to "view" the contribution in CiviCRM and look for the transaction ID.

For Stripe, you will want to search for the one that starts with "ch_" (which
stands for charge). In PayPal there should only be one transaction id.

Then login to either PayPal or Stripe, find the transaction, and choose the
refund option. If asked to enter a reason, choose "duplicate" or whatever
option best describes the reason for the refund.

After refunding in PayPal or Stripe, the status of the contribution should be
automatically reflected in CiviCRM. You do *not* need to update the record in
CiviCRM.

Lastly,

1. Be sure to send the bookkeeper

The date, amount, transaction ID and name of member for the original payment
*and* the refunded payment so it can be properly entered into the accounting
software.

2. Be sure to adjust the membership expiration date. 

When a member makes two payments, their expiration date will be extended for
two terms. If you refund one payment, *you must edit the membership and change
the expiration data back one term.* Otherwise they get the benefit of paying
twice while only paying once. Note: this is also true if they are canceling. We
use the expiration date to keep statistics on how many members are joining and
how many are leaving. We rely on an accurate expiration date to have accurate
numbers.
