+++
date = 2022-11-24T13:53:34Z
title = "Introducing May First to the Movement"
categories = ["membership"]
+++

### Being in movement spaces

We can't always wait for folks to come to us - it's important to go to
conferences, webinars, and other spaces where movement folks are gathering and
letting people know we exist and are available to answer questions and support
the movement.

### Joining the mailing list

Our [CiviCRM](/civicrm) installation has a "Universe" group that we send
general announcements and updates about our work. These announcements go to
everyone in the database with an email address that has not opted out. So,
anyone who is interested in May First but is not ready to join, can be added to
our database and will receive regular reminders about our activities.

### What's included with our membership? Can I do X?

We often get questions sent to our `info@` account from people interested in
joining. Often the questions revolve around hosting benefits. Sometimes these
questions are very basic and other times they can be highly technical and
require a technical staff person to be present.

### Can I talk to someone?

Other times, members want to talk to a human to gauge whether or not they want
to commit to a membership. Committing your Internet resources to a new
organization is a big and consequential leap that's hard to evaluate based on a
web site.


