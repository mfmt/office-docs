+++
date = 2024-07-10T13:53:34Z
title = "Anonymizing Payments"
categories = ["payments"]
+++

May First members may request that their payments be anonymized.

To anonymize a payment, in CiviCRM:

 * Ensure any in progress recurring contributions have been cancelled.
 * View the payment, if a copy of a check is attached, delete the attachment.
 * Find the contribution in CiviCRM, click the "..." option, and choose "Anonymize Contribution" 
 * Review the member contact record to ensure no traces of the contribution remain
 * If the contribution has been imported into GnuCash, manually transfer the payment to the Anonymous Customer.

![](anonymize-contribution.png)

## How does it work?

The script will move the contribution, payment activity type and contribution
activity type to the contact with the name "Anonymous Anonymous". It will also
ensure any previous recurring contributions are deleted. And any email activity
(e.g. receipt) or membership update activity is deleted.

*But*, the contribution record will remain, which will be a link to the person
who paid it.

By anonymizing a contribution, we are severing the link *between the payment
and the membership it paid for*, we are not severing the link between the
person who made the payment and the payment. We cannot sever that link because
it can always be traced via the banking system.
