+++
date = 2022-11-24T13:53:34Z
title = "Payments Out"
categories = ["payments"]
+++

## Overview

Most of the money May First spends is automatically disbursed:

 * *[Payroll](/payroll)* - For US employees, we use the Sure Payroll service
   which requires a human to login and approve the payroll twice a month. In
   addition a payroll fee is automatically deducted from our account monthly.
   For international employees, we transfer funds via Citi Mobile manually once
   a month and pay a small transfer fee. This is not categorized via CiviCRM,
   it is entered directly in our accounting software.

 * *Telehouse* - The colocation center housing most of our servers (space and
   electricity). Our Citibank account automatically mails a check to Telehouse
   every month for the amount we owe. Category: Colocation Fees.

 * *Hurricane Electric* - The provider our Internet access to our Telehouse
   servers. Our Citibank account automatically mails a check once a month.
   Category: Colocation Fees.

 * *Leased servers* - We have several leased servers - all of which are paid
   automatically via PayPal: US Dedicated, Vultr (worst name ever), objx, and
   easyVPS/easyDNS. Some are paid annually, some paid monthly. Most are PayPal.
   Category: Colocation Fees.

 * *Twilio* - provides a voice mail number, SMS messaging and a link to our
   signal account. This cost is billed irregularly, every time our balance
   falls below $10. Category: Telephone and Fax.

 * *Deepl* - machine translation service. Paid annually with our credit card.
   Category: Dues and Subscriptions.

 * *Virtual Post* - post service. We received mostly checks but also other
   administrative information related to May First. We received invoices every
   time we are charged for the service. Automatic payment is set using our
   credit card. Category: Postage and Delivery.

Additionally, we have common one-off payments or regular payments that must be made manually:

 * *Interpreters* - Our interpreters send us invoices which have to be paid via
   the payment method preferred by the Interpreter. That is typically a check
   (we send these via our Citibank account). Category: Interpretation.

 * *Hard drives* - We replace anywhere from 2 - 8 hard drives per year. We
   periodically replendish these so we always have enough on hand. Category:
   Hard Drives.

 * *Lodging* and *Food and Entertainment* - As needed. Categories: Lodging or
   Food and Entertainment.

 * *ARIN* -  IP address registry, we can pay with check or credit card
   following the instructions in the invoice. Paid annually. Category in
   CiviCRM: ARIN Fees

 * *Social Coop* - they host our Mastodon account, we pay over Open Collective
   by credit card. Paid annually. Category: Dues and Subscriptions.

 * *Domains* - we have different domain names most of them using Name.com
   services but also Network Solutions. For some of them we paid several years
   in advanced. We can use paypal or credit according to possibilities.
   Category: Domain Name Registration.

 * *Spamhouse* - service provider to track email spammers and spam-related
   activity. Paid annually with Paypal. Category: Dues and
   Subscriptions.

 * *Insurance* - Paid annually with credit card. Category: Insurance

 * *USF Coop* - we are part of the US Federation of Worker Cooperatives. Paid
   annually with credit card. Category: Dues and Subscriptions.

 * *MacinCloud.com* - mac emulated service related to technical support. Paid
   annually with Paypal. Category: Dues and Subscriptions.

 * *990 online* - We use an online service to pay our federal US taxes. Paid
   annually with Paypal. Category: Professional Fees.

 * *NYC Charities* - We pay an anual tax filing fee to the state of New York.
   Paid annually with credit card. Category: Miscellaneous.

 * *APC* - members of Association for Progressive Communications. We annually
   need to cover this amount and add it as paid by Citibank but since APC is
   also part of May First and our dues are equal actually do an exchange.
   Category: Dues and Subscriptions.

## Making the payments

We typically make payments in one of two ways:

 * Via Debit Card (comes directly out of Citibank account)
 * Via [Bank transfer](/bank-transfer) (comes directly out of Citibank account)
 * Via PayPal


Since we still receive considerable dues via PayPal, we have plenty of money in
our account to pay for things. However, it doesn't really matter which one.

## Entering the details

All payments that go out should be configured to send an invoice to the
`billing@` address. All payments should have some paper work associated with
them that gets recorded.

Whenever an invoice arrives:

 * Open our CiviCRM instance
 * Click `Contacts -> Enter May First Expense`
 * Enter the details and hit submit
 * You are redirected to the saved Activity
 * Click the Attachments section and upload the receipt.

 See our [GnuCash](/gnucash) page for how this information is entered into our accounting system.
