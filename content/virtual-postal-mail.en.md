+++
date = 2023-01-26T13:53:34Z
title = "Virtual Postal Mail"
categories = ["payments"]
+++

We maintain the following physical mailing address:

> May First Movement Technology
> 440 N BARRANCA AVE #4402 
> COVINA, CA 91723

When a letter arrives, we are notified by email and we can see a scan of the
outside of the envelope by logging in to [Virtual Post
Mail](https://virtualpostmail.com/).

If don't need to read the letter, we can trash it for free.

If we want to read the letter, we can choose to scan it (we are charged per scan).

If the letter contains a check: 

 * Scan it first and then [apply it to the appropriate account](/civicrm).

 * Keep the check in the inbox. Do not archive it.

 * Once a month or so, we ask that all checks collected be mailed to our bank
   to be deposited. Since we pay for every envelope sent to our bank, we
   typically wait for 10 or more checks to collect before we deposit them. When
   we are ready for the deposit, we send an email to:
   `deposit@virtualpostmail.com` and also cc: the person doing our book keeping
   (e.g. Jamie). See below for the template to use.

 * Wait for confirmation that the checks have been sent.

 * Delete the checks.

**Note:** If you "archive" something, the scan will be kept but the letter (or
check) will be shredded. Typically, we should never archive anything. Either
delete it (not a check) or, if a check, keep it until it has been deposited and
then delete it.

## Email to request checks be mailed:

```
Dear VPM,
Please deposit the following checks on my behalf.
Mailbox number: #4402
Mailbox Location: Covina
Bank: Citibank
Account number: [look up in keyringer]
Name on account: May First Movement Technology
Item number: [lookup in virtualpostmail.com for the given check scan] 
Check amount: 10.00
[repeat for each check]
Total deposit: [total]
```

