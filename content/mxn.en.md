+++
date = 2022-11-24T13:53:34Z
title = "Members who pay in MXN (Mexican Pesos)"
categories = ["membership"]
+++

Our members have the option of paying in USD or MXN. May First Movement
Technology only handles payments made in USD. And, the independent Mexican Coop
Primero de Mayo/Enlace Popular (PM/EP) handles payments made in MXN.

PM/EP is responsible for the accounting of MXN payments, sending receipts or
facturas and communicating about any questions or problems related to MXN
payments. Any questions from members paying in MXN should be forwarded to the
`miemtros@` email address.

However, May First Movement Technology takes responsibility for disabling any
member that falls behind in their dues, regardless of how the pay.
